<?

/*

Информация о курсе

numberGroup - номер группы
nameGroup - наименование группы
codeGroup - уникальный код курса для указния связи в счетах, заявках и т.д.
status - статус записи, 10 - действует, 70-удалено, 20 - архив, 30-сформировано и не опубликовано
name - наименование курса
period - перечень периодов проведения
	beginDate - дата начала курса
	endDate - дата окончания курса
	beginDateReguest - дата начала подачи заявки
	endDateReguest - дата окончания подачи заявки
price - стоимость в рублях
img - рисунок
categoryListener - категория слушателей
duration - продолжительность курса в часах
result - результат окончания курсов (диплом и т.д.)
formAction - форма проведения
curator - куратор курса
	name - ФИО
	skill - квалификация
teacher - перечень преподавателей курса
	name - ФИО
	skill - квалификация
linkBlank - URL файл бланка заявки
linkInfoCourse - URL файл содержание курса
ochnoEducation - наличие возможности очного обучения
	name - Наименование
	linkBlank - URL файл бланка заявки
program - программа курса
	name - пункт программы
infoCourse - информация о курсе
	name - абзац
	img - рисунок
	link - ссылка
		name - наименование
		url - адрес

*/

$course['numberGroup']=1; 
$course['nameGroup']='Группа 01';
$course['status']=10;  //10 - действует, 70-удалено, 20 - архив, 30-сформировано и не опубликовано
$course['status']=10;

$courseList['0']['nameGroup']='Группа 01';
$courseList['1']['nameGroup']='Группа 02';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';
$courseList['2']['nameGroup']='Группа 03';

//print_r($courseList);

$courseListJSON = json_encode($courseList);

echo $courseListJSON;


?>